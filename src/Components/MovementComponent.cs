using System;

namespace MonoGame_Test
{
  public class MovementComponent : BaseComponent
  {
    public float Speed
    {
      get; set;
    }
  }
}