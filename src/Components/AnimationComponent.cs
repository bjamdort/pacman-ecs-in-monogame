using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame_Test
{
  public class AnimationComponent : BaseComponent
  {
    public Texture2D Texture;
    public Animation CurrentAnimation;
    public Dictionary<string, Animation> Animations = new Dictionary<string, Animation>();
  }
}