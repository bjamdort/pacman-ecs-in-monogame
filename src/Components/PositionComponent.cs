using System;
using Microsoft.Xna.Framework;

namespace MonoGame_Test
{
  public class PositionComponent : BaseComponent
  {
    public float X
    {
      get
      {
        return _position.X;
      }
      set
      {
        _position.X = value;
      }
    }

    public float Y
    {
      get
      {
        return _position.Y;
      }
      set
      {
        _position.Y = value;
      }
    }

    private Vector2 _position;
    public Vector2 Position
    {
      get
      {
        return _position;
      }
      set
      {
        _position = value;
      }
    }
  }
}