using System;
using Microsoft.Xna.Framework;

namespace MonoGame_Test
{
    public class AnimationFrame
    {
        public Rectangle SourceRectangle { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
