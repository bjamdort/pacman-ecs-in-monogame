using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoGame_Test
{
  public class AnimationSystem : BaseSystem
  {

    public AnimationSystem(EntityManager entityManager) : base(entityManager)
    {
    }

    public override void Update(GameTime gameTime)
    {
      foreach (var entity in entityManager.GetAllEntities<AnimationComponent>())
      {
        AnimationComponent animationComponent = entity.Value as AnimationComponent;

        double secondsIntoAnimation =
            animationComponent.CurrentAnimation.timeIntoAnimation.TotalSeconds + gameTime.ElapsedGameTime.TotalSeconds;

        double remainder = secondsIntoAnimation % animationComponent.CurrentAnimation.Duration.TotalSeconds;

        animationComponent.CurrentAnimation.timeIntoAnimation = TimeSpan.FromSeconds(remainder);
      }
    }
  }
}
