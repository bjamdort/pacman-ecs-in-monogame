using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoGame_Test
{
  public class MovementSystem : BaseSystem
  {

    public MovementSystem(EntityManager entityManager) : base(entityManager)
    {
    }

    public override void Update(GameTime gameTime)
    {
      foreach (var entity in entityManager.GetAllEntities<MovementComponent>())
      {
        MovementComponent movementComponent = entity.Value as MovementComponent;
        PositionComponent positionComponent = entityManager.GetEntity<PositionComponent>(entity.Key);
        AnimationComponent animationComponent = entityManager.GetEntity<AnimationComponent>(entity.Key);
        InputComponent inputComponent = entityManager.GetEntity<InputComponent>(entity.Key);

        // Move Character
        positionComponent.Position = new Vector2(
          positionComponent.X + inputComponent.DirectionInput.X * movementComponent.Speed,
          positionComponent.Y + inputComponent.DirectionInput.Y * movementComponent.Speed
        );

        // TODO: Check if correct place for setting the animation.
        // Set animation
        if (inputComponent.DirectionInput != Vector2.Zero)
        {
          if (Math.Abs(inputComponent.DirectionInput.X) > Math.Abs(inputComponent.DirectionInput.Y))
          {
            if (inputComponent.DirectionInput.X > 0)
              animationComponent.CurrentAnimation = animationComponent.Animations["right"];
            else
              animationComponent.CurrentAnimation = animationComponent.Animations["left"];
          }
          else
          {
            if (inputComponent.DirectionInput.Y > 0)
              animationComponent.CurrentAnimation = animationComponent.Animations["down"];
            else
              animationComponent.CurrentAnimation = animationComponent.Animations["up"];
          }
        }
        else
        {
          animationComponent.CurrentAnimation = animationComponent.Animations["idle"];
        }
      }
    }
  }
}