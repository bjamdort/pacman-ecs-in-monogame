using System;
using Microsoft.Xna.Framework;

namespace MonoGame_Test
{
  public abstract class BaseSystem
  {
    protected EntityManager entityManager
    {
      get; set;
    }

    public BaseSystem(EntityManager entityManager) {
      this.entityManager = entityManager;
    }

    public abstract void Update(GameTime gameTime);
  }
}