using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame_Test
{
  public class DrawSystem : BaseSystem
  {
    private SpriteBatch _spriteBatch;

    public DrawSystem(EntityManager entityManager, SpriteBatch spriteBatch) : base(entityManager)
    {
      _spriteBatch = spriteBatch;
    }

    public override void Update(GameTime gameTime)
    {
      foreach (var entity in entityManager.GetAllEntities<AnimationComponent>())
      {
        AnimationComponent animationComponent = entity.Value as AnimationComponent;
        PositionComponent positionComponent = entityManager.GetEntity<PositionComponent>(entity.Key);

        Vector2 location = new Vector2(positionComponent.X, positionComponent.Y);
        Color tintColor = Color.White;
        Rectangle sourceRectangle = animationComponent.CurrentAnimation.CurrentRectangle;
        _spriteBatch.Draw(animationComponent.Texture, location, sourceRectangle, tintColor);
      }
    }
  }
}