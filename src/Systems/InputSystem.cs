using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoGame_Test
{
  public class InputSystem : BaseSystem
  {
    public InputSystem(EntityManager entityManager) : base(entityManager)
    {
    }

    public override void Update(GameTime gameTime)
    {
      foreach (var entity in entityManager.GetAllEntities<InputComponent>())
      {
        InputComponent inputComponent = entity.Value as InputComponent;
        KeyboardState state = Keyboard.GetState();

        float velX = 0;
        float velY = 0;

        if (state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.W))
          velY -= 1;
        else if (state.IsKeyDown(Keys.Down) || state.IsKeyDown(Keys.S))
          velY += 1;
        else if (state.IsKeyDown(Keys.Right) || state.IsKeyDown(Keys.D))
          velX += 1;
        else if (state.IsKeyDown(Keys.Left) || state.IsKeyDown(Keys.A))
          velX -= 1;

        if (velX != 0 || velY != 0)
        {
          inputComponent.DirectionInput = new Vector2(velX, velY);
        }
      }
    }
  }
}