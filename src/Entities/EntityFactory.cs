using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame_Test
{
  public class EntityFactory
  {
    private EntityManager _entityManager;

    public EntityFactory(EntityManager entityManager)
    {
      this._entityManager = entityManager;
    }
    public int CreatePlayer(float x, float y, ContentManager content)
    {
      Dictionary<string, Animation> animations = new Dictionary<string, Animation>();

      animations["right"] = new Animation();
      animations["right"].AddFrame(new Rectangle(0, 0, 16, 16), TimeSpan.FromSeconds(0.075));
      animations["right"].AddFrame(new Rectangle(16, 0, 16, 16), TimeSpan.FromSeconds(0.075));

      animations["left"] = new Animation();
      animations["left"].AddFrame(new Rectangle(0, 16, 16, 16), TimeSpan.FromSeconds(0.075));
      animations["left"].AddFrame(new Rectangle(16, 16, 16, 16), TimeSpan.FromSeconds(0.075));

      animations["up"] = new Animation();
      animations["up"].AddFrame(new Rectangle(0, 32, 16, 16), TimeSpan.FromSeconds(0.075));
      animations["up"].AddFrame(new Rectangle(16, 32, 16, 16), TimeSpan.FromSeconds(0.075));

      animations["down"] = new Animation();
      animations["down"].AddFrame(new Rectangle(0, 48, 16, 16), TimeSpan.FromSeconds(0.075));
      animations["down"].AddFrame(new Rectangle(16, 48, 16, 16), TimeSpan.FromSeconds(0.075));

      animations["idle"] = new Animation();
      animations["idle"].AddFrame(new Rectangle(32, 0, 16, 16), TimeSpan.FromSeconds(1));

      Texture2D texture = content.Load<Texture2D>("Pacman");

      return _entityManager.CreateEntity(
        new InputComponent(),
        new PositionComponent { X = x, Y = y },
        new MovementComponent { Speed = 1.2f },
        new AnimationComponent { Animations = animations, Texture = texture }
      );
    }
  }
}