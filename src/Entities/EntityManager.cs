using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace MonoGame_Test
{
  public class EntityManager
  {
    private int _lowestAssignedEntityId = -1;
    private List<int> _entities;

    private Dictionary<string, Dictionary<int, BaseComponent>> _componentsByClass;

    public EntityManager()
    {
      _entities = new List<int>();
      _componentsByClass = new Dictionary<string, Dictionary<int, BaseComponent>>();
    }

    public int CreateEntity(params BaseComponent[] components){
      _entities.Add(++_lowestAssignedEntityId);

      foreach (var c in components)
      {
          AddComponentToEntity(_lowestAssignedEntityId, c);
      }

      return _lowestAssignedEntityId;
    }

    public void AddComponentToEntity(int entityId, BaseComponent component)
    {
      string componentType = component.GetType().Name;
      if (!_componentsByClass.ContainsKey(componentType))
      {
        _componentsByClass[componentType] = new Dictionary<int, BaseComponent>();
      }

      _componentsByClass[componentType][entityId] = component;
    }

    public Dictionary<int, BaseComponent> GetAllEntities<T>() where T : BaseComponent
    {
      return _componentsByClass[typeof(T).Name];
    }

    public T GetEntity<T>(int id) where T : BaseComponent
    {
      return _componentsByClass[typeof(T).Name][id] as T;
    }
  }
}