﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace MonoGame_Test
{
  public class Game1 : Game
  {
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;

    EntityManager entityManager;
    EntityFactory entityFactory;
    BaseSystem[] UpdateSystems, DrawSystems;

    public Game1()
    {
      _graphics = new GraphicsDeviceManager(this);
      Content.RootDirectory = "Content";
      IsMouseVisible = true;
    }

    protected override void Initialize()
    {
      // Initialize window size
      _graphics.PreferredBackBufferWidth = 800;
      _graphics.PreferredBackBufferHeight = 600;
      _graphics.ApplyChanges();

      entityManager = new EntityManager();
      entityFactory = new EntityFactory(entityManager);

      entityFactory.CreatePlayer(200f, 200f, Content);

      UpdateSystems = CreateUpdateSystems(entityManager, entityFactory);

      base.Initialize();
    }

    protected override void LoadContent()
    {
      _spriteBatch = new SpriteBatch(GraphicsDevice);

      DrawSystems = CreateDrawSystems(entityManager, _spriteBatch);
    }

    protected override void Update(GameTime gameTime)
    {
      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
        Exit();

      foreach (var system in UpdateSystems)
      {
          system.Update(gameTime);
      }

      base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
      GraphicsDevice.Clear(Color.CornflowerBlue);

      _spriteBatch.Begin();
      foreach (var system in DrawSystems)
      {
          system.Update(gameTime);
      }
      _spriteBatch.End();

      base.Draw(gameTime);
    }

    private static BaseSystem[] CreateUpdateSystems(EntityManager manager, EntityFactory factory)
    {
      return new BaseSystem[]{
        new InputSystem(manager),
        new MovementSystem(manager),
        new AnimationSystem(manager),
      };
    }

    private static BaseSystem[] CreateDrawSystems(EntityManager manager, SpriteBatch batch)
    {
      return new BaseSystem[]{
        new DrawSystem(manager, batch)
      };
    }
  }
}
